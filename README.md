### Link to live API

You can use the follow link to access the API:

http://fullstacksolucoes.com/records/phone

### CRUD


The example phone records table has only a a few fields:

    tb_phone_records  
    =======
    id_phone_record 
    first_name  
    surnames
    emails
    telphones

The CRUD operations below act on this table.

#### Create

If you want to create a record the request can be written in URL format as: 

    POST /records/phone

You have to send a body containing a JSON object as demonstrated below:

   {"first_name":"Elliott Victor",
   "surnames":"de Sousa Chaves",
   "emails":["elliott@xyz.com","victor@mna.com"],
   "telphones":["+5583998613607","+558332319027"]}
   }

And it will return the value of the primary key of the newly created record:

    2

#### Read

To read a record from this table the request can be written in URL format as:

    GET /records/phone/1

Where "1" is the value of the primary key of the record that you want to read. It will return:

   {
        "first_name":"Elliott Victor",
        "surnames":"de Sousa Chaves",
        "emails":["elliott@xyz.com","victor@mna.com"],
        "telphones":["+5583998613607","+558332319027"]}
   }

#### Update

To update a record in this table the request can be written in URL format as:

    PUT /records/phone/1

Where "1" is the value of the primary key of the record that you want to update. Send as a body:

    {
        "first_name": "Josefino"
    }

This adjusts the title of the post. And the return value is the number of rows that are set:

    1

#### Delete

If you want to delete a record from this table the request can be written in URL format as:

    DELETE /records/phone/1

And it will return the number of deleted rows:

    1
