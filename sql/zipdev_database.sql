-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Sep 02, 2019 at 05:08 AM
-- Server version: 10.4.7-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zipdev`
--
CREATE DATABASE IF NOT EXISTS `zipdev` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `zipdev`;

-- --------------------------------------------------------

--
-- Table structure for table `tb_phone_record`
--

DROP TABLE IF EXISTS `tb_phone_record`;
CREATE TABLE IF NOT EXISTS `tb_phone_record` (
  `id_phone_record` int(10) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `surnames` varchar(50) NOT NULL,
  `emails` longtext NOT NULL,
  `telphones` longtext NOT NULL,
  PRIMARY KEY (`id_phone_record`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_phone_record`
--

INSERT INTO `tb_phone_record` (`id_phone_record`, `first_name`, `surnames`, `emails`, `telphones`) VALUES
(2, 'Buduela', 'Andrade', '["buduela@xyz.com","manuella@gmail.com"]', '["+5583999695815","+558332319027"]'),
(3, 'Canuella', 'vndrade', '["buduela@xyz.com","manuella@gmail.com"]', '["+5583999695815","+558332319027"]');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
