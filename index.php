<?php
/**
 * Created by PhpStorm.
 * User: elliott
 * Date: 8/30/19
 * Time: 10:39 AM
 */

require_once 'config/config.php';
require_once 'config/PDOConnector.php';

require_once 'PhoneRecordModel.php';

$uri_parameters = explode('/',$_SERVER["REQUEST_URI"]);

$conn = new PDOConnector(DB_STR,DB_USER,DB_PSW);
$phone_record = new PhoneRecordModel($conn);

switch($_SERVER['REQUEST_METHOD'])
{
    case 'POST':
        if($uri_parameters[1] == "records" && $uri_parameters[2] == "phone")
        {
            $data = json_decode(file_get_contents("php://input"),true);

            //var_dump($data);
            if(!empty($data["first_name"]) &&
                !empty($data["surnames"]) &&
                !empty($data["emails"]) &&
                !empty($data["telphones"])
            )
            {
                header("Access-Control-Allow-Origin: *");
                header("Content-Type: application/json; charset=UTF-8");
                header("Access-Control-Allow-Methods: POST");
                header("Access-Control-Max-Age: 3600");
                header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");


                $record = json_decode(json_encode($data), true);

                $record["telphones"] = json_encode($record["telphones"]);
                $record["emails"] = json_encode($record["emails"]);

                $phone_record->create($record);
                //response
                http_response_code(200);
                echo json_encode(array("ID" => $conn->lastInsertId()));
            }
            else
            {
                http_response_code(400);
                echo json_encode(array("message" => "Unable to create phone record. Data is incomplete."));
            }

        }
        else {
            http_response_code(404);
            echo json_encode(
                array("message" => "Malformed URL")
            );
        }
        break;
    case 'GET':
        if($uri_parameters[1] == "records" && $uri_parameters[2] == "phone" && (is_numeric($uri_parameters[3]) && $uri_parameters[3] > 0))
        {
            header("Access-Control-Allow-Origin: *");
            header("Content-Type: application/json; charset=UTF-8");

            $phone_record->read($uri_parameters[3]);

            $data = $phone_record->getData();

            $record = array(
                "first_name" => $data["first_name"],
                "surnames" => $data["surnames"],
                "emails" => json_decode($data["emails"]),
                "telphones" => json_decode($data["telphones"])
                );

            http_response_code(200);
            echo json_encode($record);

        }
        else {
            http_response_code(404);
            echo json_encode(
                array("message" => "Malformed URL")
            );
        }
        break;
    case 'PUT':
        if($uri_parameters[1] == "records" && $uri_parameters[2] == "phone" && (is_numeric($uri_parameters[3]) && $uri_parameters[3] > 0))
        {
            header("Access-Control-Allow-Origin: *");
            header("Content-Type: application/json; charset=UTF-8");

            $input_data = json_decode(file_get_contents("php://input"),true);

            $input_data = json_decode(json_encode($input_data), true);

            $phone_record->read($uri_parameters[3]);

            $data_phone_record = $phone_record->getData();

            $record = array(
                "id" => $uri_parameters[3],
                "first_name" => $data_phone_record["first_name"],
                "surnames" => $data_phone_record["surnames"],
                "emails" => json_decode($data_phone_record["emails"]),
                "telphones" => json_decode($data_phone_record["telphones"])
            );

            if (array_key_exists('first_name', $input_data)) {
               $record["first_name"] = $input_data["first_name"];
            }

            if (array_key_exists('surnames', $input_data)) {
                $record["surnames"] = $input_data["surnames"];
            }

            if (array_key_exists('emails', $input_data)) {
                $record["emails"] = $input_data["emails"];
            }

            if (array_key_exists('telphones', $input_data)) {
                $record["telphones"] = $input_data["telphones"];
            }

            $record["telphones"] = json_encode($record["telphones"]);
            $record["emails"] = json_encode($record["emails"]);


            $phone_record->update($record);

            $dt = $phone_record->getData();

            http_response_code(200);
            echo json_encode($dt->rowCount());

        }
        else {
            http_response_code(404);
            echo json_encode(
                array("message" => "Malformed URL")
            );
        }

        break;
    case 'DELETE':

        if($uri_parameters[1] == "records" && $uri_parameters[2] == "phone" && (is_numeric($uri_parameters[3]) && $uri_parameters[3] > 0)) {
            header("Access-Control-Allow-Origin: *");
            header("Content-Type: application/json; charset=UTF-8");
            $phone_record->delete(array($uri_parameters[3]));
            $dt = $phone_record->getData();

            http_response_code(200);
            echo json_encode($dt->rowCount());

        }
        else {
            http_response_code(404);
            echo json_encode(
                array("message" => "Malformed URL")
            );
        }

        break;

    default:
        http_response_code(404);
        echo json_encode(
            array("message" => "Malformed URL")
        );
        break;
}