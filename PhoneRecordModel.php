<?php

class PhoneRecordModel
{
    /* @var PDOConnector */
    protected $db;

    protected $data;

    public function __construct(PDOConnector $db)
    {
        $this->db = $db;
    }

    public function read($id)
    {
        $this->data = $this->db->run("SELECT * FROM tb_phone_record WHERE id_phone_record = ?", [$id])->fetch();

    }
    public function create($record)
    {
        $this->data = $this->db->run("INSERT INTO tb_phone_record (first_name, surnames, emails, telphones) VALUES (:first_name, :surnames, :emails, :telphones)", $record);
    }
    public function update($record)
    {
        $this->data = $this->db->run("UPDATE tb_phone_record SET first_name=:first_name, surnames=:surnames, emails=:emails, telphones=:telphones WHERE id_phone_record=:id", $record);
    }
    public function delete($record)
    {
        $this->data = $this->db->run("DELETE FROM tb_phone_record WHERE id_phone_record=:id", $record);
    }

    public function getData()
    {
        return $this->data;
    }
}